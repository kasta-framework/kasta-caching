package org.kastaframework.caching;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Default implementation of toString based
 * cache key generator.
 *
 * @author Sabri Onur Tuzun
 * @since 07.10.2013 10:26
 */
public class ToStringStyleBasedCacheKeyGenerator implements ToStringBasedCacheKeyGenerator {

    /**
     * Generates string representation of cache key
     * object using toStringStyle object that is prepared before.
     *
     * @param keyObject cache key object
     * @return cache key
     */
    @Override
    public String generateKey(Object keyObject, ToStringStyle toStringStyle) {
        return ToStringBuilder.reflectionToString(keyObject, toStringStyle);
    }

    /**
     * Generates string cache key using cache key objects'
     * toString() method. It uses default styling for tostringstyle object.
     *
     * @param keyObject cache key object
     * @return cache key
     */
    @Override
    public String generateKey(Object keyObject) {
        return generateKey(keyObject, ToStringStyle.DEFAULT_STYLE);
    }
}
