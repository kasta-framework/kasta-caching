package org.kastaframework.caching;

import org.kastaframework.io.ReflectionHelper;

import java.lang.reflect.Method;

/**
 * Class method defined reflective cache support.
 *
 * @author Sabri Onur Tuzun
 * @since 05.01.2014 17:52
 */
public class ReflectiveCacheSupportImpl extends CacheSupportDecorator implements ReflectiveCacheSupport {

    protected Method method;
    protected ReflectionHelper reflectionHelper;

    /**
     * Constructor.
     *
     * @param cacheSupport cacheSupport
     */
    public ReflectiveCacheSupportImpl(CacheSupport cacheSupport, Method method, ReflectionHelper reflectionHelper) {
        super(cacheSupport);
        this.method = method;
        this.reflectionHelper = reflectionHelper;
    }

    /**
     * Returns method.
     *
     * @return method
     */
    public Method getMethod() {
        return method;
    }

    /**
     * Returns reflection helper.
     *
     * @return reflection helper
     */
    public ReflectionHelper getReflectionHelper() {
        return reflectionHelper;
    }

    /**
     * Checks cache if exists, if not exists reflects method and
     * return element. Invokes class defined method using reflection.
     *
     * @param name                name
     * @param key                 key
     * @param reflectedObject     object
     * @param reflectedObjectArgs arguments
     * @return element
     */
    @Override
    public Object reflectAndReturnElement(String name, Object key, Object reflectedObject, Object... reflectedObjectArgs) {
        if (containsKey(name, key)) {
            return get(name, key);
        }
        Object value = reflectionHelper.invokeMethod(method, reflectedObject, reflectedObjectArgs);
        return putAndReturnElement(name, key, value);
    }

    /**
     * First generates cache key and
     * checks cache if exists, if not exists reflects method and
     * return element.
     *
     * @param name                name
     * @param keyObject           key object
     * @param keyGenerator        cache key generator
     * @param reflectedObject     object
     * @param reflectedObjectArgs arguments
     * @return element
     */
    @Override
    public Object reflectAndReturnElementUsingKeyGenerator(String name, Object keyObject, CacheKeyGenerator keyGenerator, Object reflectedObject, Object... reflectedObjectArgs) {
        Object key = keyGenerator.generateKey(keyObject);
        return reflectAndReturnElement(name, key, reflectedObject, reflectedObjectArgs);
    }
}
