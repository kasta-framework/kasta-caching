package org.kastaframework.caching;

/**
 * Throwable if cache is not found.
 *
 * @author Sabri Onur Tuzun
 * @since 05.12.2013 16:04
 */
public class CacheNameNotFoundException extends CachingException {

    /**
     * Formatted constructor with not found cache name.
     *
     * @param name name
     */
    public CacheNameNotFoundException(String name) {
        super("Cache name cannot be found for given cache name : %s", name);
    }
}
