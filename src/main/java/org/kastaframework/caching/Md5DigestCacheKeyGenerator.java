package org.kastaframework.caching;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Predefined digest cache key generator.
 *
 * @author Sabri Onur Tuzun
 * @since 07.10.2013 10:28
 */
public class Md5DigestCacheKeyGenerator implements StringCacheKeyGenerator {

    /**
     * Generates md5 cache key.
     *
     * @param keyObject cache key object
     * @return cache key
     */
    @Override
    public String generateKey(Object keyObject) {
        return DigestUtils.md5Hex(keyObject.toString());
    }
}
