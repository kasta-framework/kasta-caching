package org.kastaframework.caching;

import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Simply generates string cache key using
 * cache key objects' toString() method.
 *
 * @author Sabri Onur Tuzun
 * @since 08.08.2014 14:59
 */
public interface ToStringBasedCacheKeyGenerator extends StringCacheKeyGenerator {

    /**
     * Generates string representation of cache key
     * object using toString method.
     *
     * @param keyObject cache key object
     * @return cache key
     */
    String generateKey(Object keyObject, ToStringStyle toStringStyle);
}
