package org.kastaframework.caching;

/**
 * String cache key generator provides generation
 * of cache key as java.lang.String.
 *
 * @author Sabri Onur Tuzun
 * @since 22.04.2014 17:02
 */
public interface StringCacheKeyGenerator extends CacheKeyGenerator {

    /**
     * Provides generation of string cache key.
     * This method returns java.lang.String as key type. It takes
     * key object and translates it into string representation.
     *
     * @param keyObject cache key object
     * @return string cache key
     */
    @Override
    String generateKey(Object keyObject);
}
