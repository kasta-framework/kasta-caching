package org.kastaframework.caching;

/**
 * Reflective cache support.
 *
 * @author Sabri Onur Tuzun
 * @since 05.01.2014 17:13
 */
public interface ReflectiveCacheSupport extends CacheSupport {

    /**
     * Checks cache if exists, if not exists reflects method and
     * return element.
     *
     * @param name                name
     * @param key                 key
     * @param reflectedObject     object
     * @param reflectedObjectArgs arguments
     * @return element
     */
    Object reflectAndReturnElement(String name, Object key, Object reflectedObject, Object... reflectedObjectArgs);

    /**
     * First generates cache key and
     * checks cache if exists, if not exists reflects method and
     * return element.
     *
     * @param name                name
     * @param keyObject           key object
     * @param keyGenerator        cache key generator
     * @param reflectedObject     object
     * @param reflectedObjectArgs arguments
     * @return element
     */
    Object reflectAndReturnElementUsingKeyGenerator(String name, Object keyObject, CacheKeyGenerator keyGenerator, Object reflectedObject, Object... reflectedObjectArgs);
}
