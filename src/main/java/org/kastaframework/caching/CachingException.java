package org.kastaframework.caching;

import org.kastaframework.core.exception.BaseFormattedRuntimeException;

/**
 * Base cache exception.
 *
 * @author Sabri Onur Tuzun
 * @since 01.12.2013 20:47
 */
public class CachingException extends BaseFormattedRuntimeException {

    /**
     * Constructor with message and arguments.
     *
     * @param message   message
     * @param arguments arguments
     */
    public CachingException(String message, Object... arguments) {
        super(message, arguments);
    }

    /**
     * Constructor with throwable cause.
     *
     * @param message   message
     * @param cause     cause
     * @param arguments arguments
     */
    public CachingException(String message, Throwable cause, Object... arguments) {
        super(message, cause, arguments);
    }
}
