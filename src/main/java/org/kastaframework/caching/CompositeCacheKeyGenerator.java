package org.kastaframework.caching;

import java.util.List;

/**
 * Composite cache key generator.
 *
 * @author Sabri Onur Tuzun
 * @since 08.08.2014 13:59
 */
public class CompositeCacheKeyGenerator implements CacheKeyGenerator {

    protected List<CacheKeyGenerator> cacheKeyGenerators;

    /**
     * Constructs cache key generators that are used
     * to generate cache key.
     *
     * @param cacheKeyGenerators list of cache key generators
     */
    public CompositeCacheKeyGenerator(List<CacheKeyGenerator> cacheKeyGenerators) {
        this.cacheKeyGenerators = cacheKeyGenerators;
    }

    /**
     * Returns prepared cache key generators.
     *
     * @return cache key generators
     */
    public List<CacheKeyGenerator> getCacheKeyGenerators() {
        return cacheKeyGenerators;
    }

    /**
     * Generates cache key using cache key generators setted.
     * This method uses first non null cache key generator to
     * generate cache key.
     *
     * @param keyObject cache key object
     * @return cache key
     */
    @Override
    public Object generateKey(Object keyObject) {
        for (CacheKeyGenerator cacheKeyGenerator : cacheKeyGenerators) {
            if (cacheKeyGenerator != null) {
                return cacheKeyGenerator.generateKey(keyObject);
            }
        }
        return null;
    }
}
