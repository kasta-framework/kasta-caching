package org.kastaframework.caching;

import org.springframework.cache.Cache;

/**
 * Cache support that provides cache related methods.
 *
 * @author Sabri Onur Tuzun
 * @since 04.08.2014 11:21
 */
public interface CacheSupport {

    /**
     * Returns cache object.
     *
     * @param name cache name
     * @return cache
     */
    Cache getCache(String name);

    /**
     * Returns cached value using given key.
     *
     * @param name cache name
     * @param key  cache key
     * @return value
     */
    Object get(String name, Object key);

    /**
     * Puts object to cache.
     *
     * @param name  cache name
     * @param key   cache key
     * @param value value
     */
    void put(String name, Object key, Object value);

    /**
     * Puts object to cache and returns it.
     *
     * @param name  cache name
     * @param key   cache key
     * @param value value
     * @return value
     */
    Object putAndReturnElement(String name, Object key, Object value);

    /**
     * Checks if cache contains key.
     *
     * @param name cache name
     * @param key  cache key
     * @return true if contains
     */
    boolean containsKey(String name, Object key);

    /**
     * Removes object from cache.
     *
     * @param name cache name
     * @param key  cache key
     */
    void remove(String name, Object key);

    /**
     * Removes all objects in the cache.
     *
     * @param name cache name
     */
    void removeAll(String name);
}
