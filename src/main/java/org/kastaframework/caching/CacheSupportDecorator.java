package org.kastaframework.caching;

import org.springframework.cache.Cache;

/**
 * Cache support decorator.
 *
 * @author Sabri Onur Tuzun
 * @since 05.01.2014 17:06
 */
public abstract class CacheSupportDecorator implements CacheSupport {

    protected CacheSupport cacheSupport;

    protected CacheSupportDecorator(CacheSupport cacheSupport) {
        this.cacheSupport = cacheSupport;
    }

    @Override
    public Cache getCache(String name) {
        return cacheSupport.getCache(name);
    }

    @Override
    public Object get(String name, Object key) {
        return cacheSupport.get(name, key);
    }

    @Override
    public void put(String name, Object key, Object value) {
        cacheSupport.put(name, key, value);
    }

    @Override
    public Object putAndReturnElement(String name, Object key, Object value) {
        return cacheSupport.putAndReturnElement(name, key, value);
    }

    @Override
    public boolean containsKey(String name, Object key) {
        return cacheSupport.containsKey(name, key);
    }

    @Override
    public void remove(String name, Object key) {
        cacheSupport.remove(name, key);
    }

    @Override
    public void removeAll(String name) {
        cacheSupport.removeAll(name);
    }
}
