package org.kastaframework.caching;

import org.springframework.cache.jcache.JCacheCache;

/**
 * JCache cache support.
 *
 * @author Sabri Onur Tuzun
 * @since 04.08.2014 17:23
 */
public interface JCacheCacheSupport extends CacheSupport {

    /**
     * Finds jcache cache instance from cache manager.
     *
     * @param name cache name
     * @return jcache cache
     */
    JCacheCache getJCacheCache(String name);

    /**
     * Finds native javax.cache.Cache instance from cache manager.
     *
     * @param name cache manager.
     * @return native javax.cache.Cache cache
     */
    javax.cache.Cache getNativeJCache(String name);
}
