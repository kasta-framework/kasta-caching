package org.kastaframework.caching;

import net.sf.ehcache.Ehcache;
import org.kastaframework.core.exception.ExceptionFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.ehcache.EhCacheCache;
import org.springframework.cache.ehcache.EhCacheCacheManager;

/**
 * Default ehcache cache support.
 *
 * @author Sabri Onur Tuzun
 * @since 04.08.2014 13:41
 */
public class EhCacheCacheSupportImpl extends AbstractCacheSupport implements EhCacheCacheSupport {

    /**
     * Holds configured ehcachecache manager.
     *
     * @param cacheManager     cacheManager
     * @param exceptionFactory exceptionFactory
     */
    public EhCacheCacheSupportImpl(EhCacheCacheManager cacheManager, ExceptionFactory exceptionFactory) {
        super(cacheManager, exceptionFactory);
    }

    /**
     * Finds ehcache cache instance from cache manager.
     *
     * @param name cache name
     * @return ehcache cache
     */
    @Override
    public EhCacheCache getEhCacheCache(String name) {
        Cache cache = getCache(name);
        return (EhCacheCache) cache;
    }

    /**
     * Finds native ehcache instance from cache manager.
     *
     * @param name cache name
     * @return native ehcache
     */
    @Override
    public Ehcache getNativeEhCache(String name) {
        return getEhCacheCache(name).getNativeCache();
    }

    /**
     * Finds native ehcache from cache manager,
     * checks if cache contains cache key.
     *
     * @param name cache name
     * @param key  cache key
     * @return true if contains
     */
    @Override
    public boolean containsKey(String name, Object key) {
        return getNativeEhCache(name).isKeyInCache(key);
    }

    /**
     * Checks if given value is in ehcache.
     *
     * @param name  cache name
     * @param value value
     * @return true is contains
     */
    @Override
    public boolean isValueInCache(String name, Object value) {
        return getNativeEhCache(name).isValueInCache(value);
    }
}
