package org.kastaframework.caching;

/**
 * Cache key generator interface.
 *
 * @author Sabri Onur Tuzun
 * @since 03.10.2013 15:45
 */
public interface CacheKeyGenerator {

    /**
     * Generates cache key from cache key object.
     *
     * @param keyObject cache key object
     * @return cache key
     */
    Object generateKey(Object keyObject);
}
