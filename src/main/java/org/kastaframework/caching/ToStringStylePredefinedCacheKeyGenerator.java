package org.kastaframework.caching;

import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Predefined toStringStyle cache key generator.
 * It uses super classes' methods to generate cache key.
 *
 * @author Sabri Onur Tuzun
 * @since 08.08.2014 14:54
 */
public class ToStringStylePredefinedCacheKeyGenerator extends ToStringStyleBasedCacheKeyGenerator {

    private ToStringStyle toStringStyle;

    /**
     * Constructs generator object with given toStringStyle object.
     * It provides easy generation from toString() method.
     *
     * @param toStringStyle toStringStyle that will be used toString invocation
     */
    public ToStringStylePredefinedCacheKeyGenerator(ToStringStyle toStringStyle) {
        this.toStringStyle = toStringStyle;
    }

    /**
     * Generates string cache key using cache key objects'
     * toString() method.
     *
     * @param keyObject cache key object
     * @return cache key
     */
    @Override
    public String generateKey(Object keyObject) {
        return generateKey(keyObject, toStringStyle);
    }
}
