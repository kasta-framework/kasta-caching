package org.kastaframework.caching;

import org.kastaframework.core.exception.ExceptionFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.jcache.JCacheCache;
import org.springframework.cache.jcache.JCacheCacheManager;

/**
 * Default jcache cache support.
 *
 * @author Sabri Onur Tuzun
 * @since 04.08.2014 17:26
 */
public class JCacheCacheSupportImpl extends AbstractCacheSupport implements JCacheCacheSupport {

    /**
     * Holds configured jcachecache manager.
     *
     * @param cacheManager     cacheManager
     * @param exceptionFactory exceptionFactory
     */
    public JCacheCacheSupportImpl(JCacheCacheManager cacheManager, ExceptionFactory exceptionFactory) {
        super(cacheManager, exceptionFactory);
    }

    /**
     * Finds jcache cache instance from cache manager.
     *
     * @param name cache name
     * @return jcache cache
     */
    @Override
    public JCacheCache getJCacheCache(String name) {
        Cache cache = getCache(name);
        return (JCacheCache) cache;
    }

    /**
     * Finds native javax.cache.Cache instance from cache manager.
     *
     * @param name cache manager.
     * @return native javax.cache.Cache cache
     */
    @Override
    public javax.cache.Cache getNativeJCache(String name) {
        return getJCacheCache(name).getNativeCache();
    }

    /**
     * Checks if cache key is exists in jcache.
     *
     * @param name cache name
     * @param key  cache key
     * @return true if contains
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean containsKey(String name, Object key) {
        return getNativeJCache(name).containsKey(key);
    }
}
