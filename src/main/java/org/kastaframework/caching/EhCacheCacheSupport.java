package org.kastaframework.caching;

import net.sf.ehcache.Ehcache;
import org.springframework.cache.ehcache.EhCacheCache;

/**
 * Ehcache cache support.
 *
 * @author Sabri Onur Tuzun
 * @since 04.08.2014 13:40
 */
public interface EhCacheCacheSupport extends CacheSupport {

    /**
     * Finds ehcache cache instance from cache manager.
     *
     * @param name cache name
     * @return ehcache cache
     */
    EhCacheCache getEhCacheCache(String name);

    /**
     * Finds native ehcache instance from cache manager.
     *
     * @param name cache name
     * @return native ehcache
     */
    Ehcache getNativeEhCache(String name);

    /**
     * Checks if given value is in ehcache.
     *
     * @param name  cache name
     * @param value value
     * @return true is contains
     */
    boolean isValueInCache(String name, Object value);
}
