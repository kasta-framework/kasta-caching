package org.kastaframework.caching;

import org.kastaframework.core.exception.ExceptionFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

/**
 * Base caching support.
 *
 * @author Sabri Onur Tuzun
 * @since 04.08.2014 11:23
 */
public abstract class AbstractCacheSupport implements CacheSupport {

    protected CacheManager cacheManager;
    protected ExceptionFactory exceptionFactory;

    /**
     * Holds configures cache manager.
     *
     * @param cacheManager     cacheManager
     * @param exceptionFactory exceptionFactory
     */
    protected AbstractCacheSupport(CacheManager cacheManager, ExceptionFactory exceptionFactory) {
        this.cacheManager = cacheManager;
        this.exceptionFactory = exceptionFactory;
    }

    /**
     * Returns cache from cache manager.
     * If not found, it throws cache name not found exception.
     *
     * @param name cache name
     * @return cache
     */
    @Override
    public Cache getCache(String name) {
        Cache cache = cacheManager.getCache(name);
        exceptionFactory.throwIfNull(cache, CacheNameNotFoundException.class, name);
        return cache;
    }

    /**
     * Returns cached valu using given key.
     *
     * @param name cache name
     * @param key  cache key
     * @return value
     */
    @Override
    public Object get(String name, Object key) {
        return getCache(name).get(key);
    }

    /**
     * Puts object to cache.
     *
     * @param name  cache name
     * @param key   cache key
     * @param value value
     */
    @Override
    public void put(String name, Object key, Object value) {
        getCache(name).put(key, value);
    }

    /**
     * Puts object to cache and returns it.
     *
     * @param name  cache name
     * @param key   cache key
     * @param value value
     * @return value
     */
    @Override
    public Object putAndReturnElement(String name, Object key, Object value) {
        put(name, key, value);
        return value;
    }

    /**
     * Removes object from cache.
     *
     * @param name cache name
     * @param key  cache key
     */
    @Override
    public void remove(String name, Object key) {
        getCache(name).evict(key);
    }

    /**
     * Removes all objects in the cache.
     *
     * @param name cache name
     */
    @Override
    public void removeAll(String name) {
        getCache(name).clear();
    }
}
